CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provide a dialog to add images gallery into CKEditor field.

 * Select images via Imce dialog

 * Able to modify the Alt, Title tag of a image

 * Able to add multiple images, multiple gallery to CKEditor field

 * Able to drag & drop to re-order image

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ckeditor_images_gallery

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ckeditor_images_gallery


REQUIREMENTS
------------

This module requires the following modules:

 * Imce (https://www.drupal.org/project/imce)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Configure the button in
   Configuration » Content authoring » Text formats and editors » Full HTML

 * Drag the button from Available buttons section to Active toolbar section

 * After save the form, you will see new button on your CKEditor field

 MAINTAINERS
 -----------

 Current maintainers:

  * Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
